<?php
include "sistema/conexao.php";

if(!$_COOKIE["id_usuario"]) {
    echo"<script>window.location='index.php?msg=".urlencode('A sess&atilde;o expirou! Fa&ccedil;a login novamente.')."'</script>";
} else {
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Clinic</title>
	<meta charset="utf-8">
	<meta name="language" content="portuguese"/>
    <meta name="revisit-after" content="2 days"/>
    <meta name="document-state" content="Dynamic"/>
    <meta name="document-distribution" content="Global"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="msapplication-navbutton-color" content="#dee2e8"/>
    <meta name="theme-color" content="#dee2e8">
    <meta name="apple-mobile-web-app-status-bar-style" content="#dee2e8">
    <meta name="msapplication-navbutton-color" content="#dee2e8">

	<link rel="shortcut icon" href="<?= $caminhoImg ?>/cropped-favicon.ico" />
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="<?= $CAMINHOCSS ?>/metismenu.css">
	<link rel="stylesheet" type="text/css" href="<?= $CAMINHOCSS ?>/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?= $CAMINHOCSS ?>/fancybox/jquery.fancybox.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">
	<link rel="stylesheet" type="text/css" href="<?= $CAMINHOCSS ?>/bootstrap-datepicker.min.css">
	<link rel="stylesheet" type="text/css" href="<?= $CAMINHOCSS ?>/menu_sidebar.css">
	<link rel="stylesheet" type="text/css" href="<?= $CAMINHOCSS ?>/style.css">


	<script type="text/javascript" src="<?= $CAMINHOJS ?>/jquery-1.11.3.min.js"></script>
	<script src="https://igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js"></script> 
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script type="text/javascript" src="<?= $CAMINHOJS ?>/jquery.validate.js"></script>
	<script type="text/javascript" src="<?= $CAMINHOJS ?>/jquery.maskMoney.js"></script>
	<script type="text/javascript" src="<?= $CAMINHOJS ?>/jquery.noty.packaged.js"></script>
	<script type="text/javascript" src="<?= $CAMINHOJS ?>/jquery.fancybox-1.3.4.js"></script>
	<script type="text/javascript" src="<?= $CAMINHOJS ?>/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?= $CAMINHOJS ?>/metismenu.js"></script>
	<script type="text/javascript" src="<?= $CAMINHOJS ?>/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?= $CAMINHOJS ?>/bootstrap-datepicker.pt-BR.min.js"></script>
	<script src="http://cdn.ckeditor.com/4.5.4/full/ckeditor.js"></script>
	<script src="http://cdn.ckeditor.com/4.5.4/full/adapters/jquery.js"></script>
	<script type="text/javascript" src="<?= $CAMINHOJS ?>/funcoes.js"></script>
	<script type="text/javascript" src="<?= $CAMINHOJS ?>/mais.js"></script>


	<style type="text/css">
		table {
  
       border-collapse: separate!important;
       border-spacing: 1px !important;
 
	}

	</style>


</head>

<body>
	<script type="text/javascript">
		$('.dinheiro').mask('#.##0,00', {reverse: true});
	</script>
	
		<div class="wrapper">
			
	      	<?php include "sistema/menu.php"; ?>

			
			<div id="content">

				<nav class="navbar navbar-default ">
                    <div class="container-fluid" style="background: #dee2e8">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="navbar-btn">
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
						        <button type="button"
										id="abre_menu"
										class="btn btn-link link_menu dropdown-toggle hidden-xs"
										data-toggle="dropdown"
						        		aria-haspopup="true"
						        		aria-expanded="false">
										<i class="fa fa-info-circle" style="color: #ffffff;"></i>
								</button>
								
								<ul class="dropdown-menu dropdown-menu-right">
						        	<li><a href="#"><strong><?= $idCliente ?></strong> | <?= $buscaCliente['razao_social'] ?></a></li>
						        	<li role="separator" class="divider"></li>
						        	<li><a href="#"><strong>Usu&aacute;rio: </strong><?= $buscaAdministrador['usuario'] ?></a></li>
						        </ul>
						     </li>
						     <li>
						     	<button type="button"
		    					class="btn btn-link link_menu"
		    					data-toggle="tooltip"
		    					data-placement="left"
		    					onclick="logoff()"
		    					title="Sair do Sistema">
		    					<i class="fa fa-sign-out" style="color: red;"></i>
								</button>
						     </li>
                                
                             
                            </ul>
                        </div>
                    </div>
                </nav>

			 <div class="col-md-12" id="conteudo" onclick="menu_lateral()">
			 	<?php include "sistema/sequencia.php"; ?>
			 </div>
								
						
				<footer>
				<div id="<?= $_GET['t'] ? "copy_interno" : "copy" ?>"><a href="http://www.hidigital.com.br" target="_blank">&copy; <?= date("Y") ?> - HIDIGITAL. - Todos os direitos reservados</a></div>
				</footer>
			</div>			
		</div>
		
	
<?

$useragent=$_SERVER['HTTP_USER_AGENT'];

$mobile = preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4));


?>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js "></script>
<script type="text/javascript">
	$(document).on('click', '[data-toggle="lightbox"]', function(event) {
		event.preventDefault();
		$(this).ekkoLightbox();
	});

	$.extend($.validator.messages, {
    required: "Este campo &eacute; obrigat&oacute;rio.",
    remote: "Por favor, corrija este campo.",
    email: "Por favor, forne&ccedil;a um endere&ccedil;o eletr&ocirc;nico v&aacute;lido.",
    url: "Por favor, forne&ccedil;a uma URL v&aacute;lida.",
    date: "Por favor, forne&ccedil;a uma data v&aacute;lida.",
    dateISO: "Por favor, forne&ccedil;a uma data v&aacute;lida (ISO).",
    number: "Por favor, forne&ccedil;a um n&uacute;mero v&aacute;lido.",
    digits: "Por favor, forne&ccedil;a somente d&iacute;gitos.",
    creditcard: "Por favor, forne&ccedil;a um cart&atilde;o de cr&eacute;dito v&aacute;lido.",
    equalTo: "Por favor, forne&ccedil;a o mesmo valor novamente.",
    accept: "Por favor, forne&ccedil;a um valor com uma extens&atilde;o v&aacute;lida.",
    maxlength: $.validator.format("Por favor, forne&ccedil;a n&atilde;o mais que {0} caracteres."),
    minlength: $.validator.format("Por favor, forne&ccedil;a ao menos {0} caracteres."),
    rangelength: $.validator.format("Por favor, forne&ccedil;a um valor entre {0} e {1} caracteres de comprimento."),
    range: $.validator.format("Por favor, forne&ccedil;a um valor entre {0} e {1}."),
    max: $.validator.format("Por favor, forne&ccedil;a um valor menor ou igual a {0}."),
    min: $.validator.format("Por favor, forne&ccedil;a um valor maior ou igual a {0}.")
});
	
</script>

<script type="text/javascript">
             $(document).ready(function () {
                 $('#sidebarCollapse').on('click', function () {
                     $('#sidebar').toggleClass('active');
                     $(this).toggleClass('active');
                 });

              
             });
            
 <? if($mobile){ ?>
 	function menu_lateral(){
             	 var isMobile = window.matchMedia("only screen and (max-width: 760px)");
             	 if(isMobile){
             	 	if(($( "#sidebar" ).hasClass('active')) == true){
                    	 $('#sidebar').toggleClass('active');
                         $('#sidebarCollapse').toggleClass('active');
                    }
             	 }
                    
     }
 <? } else { ?>
 	function menu_lateral(){
 		
 	}

 <? } ?>       

 
 </script>


</body>
</html>
<?php
}